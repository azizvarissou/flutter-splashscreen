import 'package:flutter/material.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Theme.of(context).accentColor,
        child: Container(
          child: Column(
            children: [
              Container(
                color: Colors.white,
                child: Column(
                  children: [
                    LayoutBuilder(builder: (builder, contains) {
                      if (contains.maxWidth > 500) {
                        return SizedBox(height: 10);
                      } else {
                        return SizedBox(height: 30);
                      }
                    }),
                    Container(
                      margin: EdgeInsets.all(10),
                      width: double.infinity,
                      padding: EdgeInsets.all(20),
                      child: Text(
                        'Connectez-vous',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: SafeArea(
                  child: TextField(
                    enableInteractiveSelection: true,
                    enabled: true,
                    autofocus: true,
                    autocorrect: true,
                    keyboardAppearance: Brightness.light,
                    textInputAction: TextInputAction.newline,
                    keyboardType: TextInputType.emailAddress,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.black,
                    ),
                    cursorColor: Theme.of(context).accentColor,
                    decoration: InputDecoration(
                        focusColor: Colors.white60,
                        filled: true,
                        border: InputBorder.none,
                        fillColor: Colors.white60),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
