import 'package:flutter/material.dart';
import 'package:splash_drawer/src/screens/homescreen.dart';
import 'package:splash_drawer/src/screens/loginscreen.dart';

class Splashscreen extends StatefulWidget {
  bool isConnected = false;
  @override
  _SplashscreenState createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this._check();
    _test();
  }

  Future<bool> _checkRoute() async {
    await Future.delayed(Duration(seconds: 6), () {});
    return false;
  }

  void _check() {
    _checkRoute().then(
      (value) {
        if (value) {
          return Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (BuildContext context) {
            return HomeScreen();
          }));
        } else {
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (BuildContext context) {
            return Login();
          }));
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Stack(
          children: [
            Placeholder(
              fallbackHeight: 50,
              fallbackWidth: 400,
              strokeWidth: 5,
            ),
            Center(
              child: Image(
                // AssetImage('assets/images/animation2.gif')
                image: AssetImage('assets/images/animation2.gif'),
                height: 100,
                width: 100,
              ),
            ),
          ],
        ),
      ),
    );
  }

  _test({z = 1}) {
    Person()
      ..name = "VARIS"
      ..age = 26
      ..leJson = {"name": "GHOST"}
      ..Lire()
      ..LireJson()
      ..marche();
  }
}

abstract class Humain {
  marche() {
    print("Un humain peut marcher");
  }
}

class Person extends Humain {
  String name;
  int age;
  Map<dynamic, dynamic> leJson;
  Person({this.age, this.name, this.leJson});
  Person.toJson({data = "GHOST"}) {
    this.leJson = {'name': data};
  }
  Lire() {
    print(this.name + ' avec pour age ${this.age}');
  }

  LireJson() {
    print(leJson);
  }

  @override
  marche() {
    // TODO: implement marche
    print('$name peut aussi marcher');
    //  super.marche();
  }
}
